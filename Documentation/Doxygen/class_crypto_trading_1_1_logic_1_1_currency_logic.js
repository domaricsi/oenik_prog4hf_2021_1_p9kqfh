var class_crypto_trading_1_1_logic_1_1_currency_logic =
[
    [ "CurrencyLogic", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#abb6915cf682220e7f0a1a84c35a14823", null ],
    [ "Add", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a3c7f4554ad4ae55cd8b20990c79f9d95", null ],
    [ "AddNewOne", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a10240c9faed570e6039aa1a18d08ab7f", null ],
    [ "CurrencyPrice", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a5a40d051e21ef0101adf92709817cfc2", null ],
    [ "DeleteCurrency", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a00975a1beaed5cf91156b155f3cdd8dd", null ],
    [ "GetAllNameAsString", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a14aa9f9d786ec4d26412154a885b030d", null ],
    [ "Read", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a6e11a1ed31ac7a5f9a4fdfb993e54fa7", null ],
    [ "Refresh", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a1f4013ac6f537baea449107aa05e815f", null ],
    [ "Remove", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#a872f43ebe526cf26a1f07b2ff3954f59", null ],
    [ "Update", "class_crypto_trading_1_1_logic_1_1_currency_logic.html#ab7149830fcd734bbf3b65a5bb8e1355c", null ]
];