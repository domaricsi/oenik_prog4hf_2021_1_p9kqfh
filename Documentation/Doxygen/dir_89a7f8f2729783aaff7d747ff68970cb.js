var dir_89a7f8f2729783aaff7d747ff68970cb =
[
    [ "bin", "dir_d501d145893e1efeba2fd8e9a0489594.html", "dir_d501d145893e1efeba2fd8e9a0489594" ],
    [ "obj", "dir_8f4cbe36980e8f5577a99c2cdc4a558f.html", "dir_8f4cbe36980e8f5577a99c2cdc4a558f" ],
    [ "CryptoRepository.cs", "_crypto_repository_8cs_source.html", null ],
    [ "CurrencyRepository.cs", "_currency_repository_8cs_source.html", null ],
    [ "DBRepository.cs", "_d_b_repository_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_crypto_trading_8_repository_2_global_suppressions_8cs_source.html", null ],
    [ "ICryptoRepository.cs", "_i_crypto_repository_8cs_source.html", null ],
    [ "ICurrencyRepository.cs", "_i_currency_repository_8cs_source.html", null ],
    [ "IMemberRepository.cs", "_i_member_repository_8cs_source.html", null ],
    [ "IRepository.cs", "_i_repository_8cs_source.html", null ],
    [ "MemberRepository.cs", "_member_repository_8cs_source.html", null ],
    [ "TradeItemBaseRepository.cs", "_trade_item_base_repository_8cs_source.html", null ],
    [ "TradeRepository.cs", "_trade_repository_8cs_source.html", null ]
];