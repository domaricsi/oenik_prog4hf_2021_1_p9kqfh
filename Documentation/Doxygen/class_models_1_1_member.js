var class_models_1_1_member =
[
    [ "Member", "class_models_1_1_member.html#aab19461ed4a08adc7c88c02c469c2aa8", null ],
    [ "BirthYear", "class_models_1_1_member.html#ad7d8354fcdcbe6a7a814887b946faaa5", null ],
    [ "BTCWallet", "class_models_1_1_member.html#a61c4c1ac2f4a2a79dcbb01f697b125d2", null ],
    [ "ETHWallet", "class_models_1_1_member.html#aedf28406ba20141b39f658e18fad3842", null ],
    [ "FullName", "class_models_1_1_member.html#aebfdd2a7319d6752c58ed47c88c21056", null ],
    [ "MemberID", "class_models_1_1_member.html#a8495471c2f0037d6053f24159755be3a", null ],
    [ "Money", "class_models_1_1_member.html#af32cef590731add6b847ebf10839014c", null ],
    [ "Password", "class_models_1_1_member.html#ad026ba7c9829486c39b1b8c8332bcfec", null ],
    [ "Trades", "class_models_1_1_member.html#aaa2c9dd116caabed81335956cee6189b", null ],
    [ "UserName", "class_models_1_1_member.html#a65cd12cd0fbddd0dee9d8c4bf92fa679", null ]
];