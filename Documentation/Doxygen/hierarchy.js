var hierarchy =
[
    [ "CryptoTrading.Logic.Test.CryptoRepoTest", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html", null ],
    [ "DbContext", null, [
      [ "CryptoTrading.Data.CTDDataBase", "class_crypto_trading_1_1_data_1_1_c_t_d_data_base.html", null ]
    ] ],
    [ "CryptoTrading.Repository.DBRepository< Crypto >", "class_crypto_trading_1_1_repository_1_1_d_b_repository.html", [
      [ "CryptoTrading.Repository.CryptoRepository", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html", null ]
    ] ],
    [ "CryptoTrading.Repository.DBRepository< Currency >", "class_crypto_trading_1_1_repository_1_1_d_b_repository.html", [
      [ "CryptoTrading.Repository.CurrencyRepository", "class_crypto_trading_1_1_repository_1_1_currency_repository.html", null ]
    ] ],
    [ "CryptoTrading.Repository.DBRepository< Member >", "class_crypto_trading_1_1_repository_1_1_d_b_repository.html", [
      [ "CryptoTrading.Repository.MemberRepository", "class_crypto_trading_1_1_repository_1_1_member_repository.html", null ]
    ] ],
    [ "CryptoTrading.Repository.DBRepository< Trade >", "class_crypto_trading_1_1_repository_1_1_d_b_repository.html", [
      [ "CryptoTrading.Repository.TradeRepository", "class_crypto_trading_1_1_repository_1_1_trade_repository.html", null ]
    ] ],
    [ "CryptoTrading.Repository.DBRepository< TradeItemBase >", "class_crypto_trading_1_1_repository_1_1_d_b_repository.html", [
      [ "CryptoTrading.Repository.TradeItemBaseRepository", "class_crypto_trading_1_1_repository_1_1_trade_item_base_repository.html", null ]
    ] ],
    [ "CryptoTrading.Program.Factory", "class_crypto_trading_1_1_program_1_1_factory.html", null ],
    [ "CryptoTrading.Logic.ILogic< T >", "interface_crypto_trading_1_1_logic_1_1_i_logic.html", null ],
    [ "CryptoTrading.Logic.ILogic< Crypto >", "interface_crypto_trading_1_1_logic_1_1_i_logic.html", [
      [ "CryptoTrading.Logic.ICryptoLogic", "interface_crypto_trading_1_1_logic_1_1_i_crypto_logic.html", [
        [ "CryptoTrading.Logic.CryptoLogic", "class_crypto_trading_1_1_logic_1_1_crypto_logic.html", null ]
      ] ]
    ] ],
    [ "CryptoTrading.Logic.ILogic< Currency >", "interface_crypto_trading_1_1_logic_1_1_i_logic.html", [
      [ "CryptoTrading.Logic.ICurrencyLogic", "interface_crypto_trading_1_1_logic_1_1_i_currency_logic.html", [
        [ "CryptoTrading.Logic.CurrencyLogic", "class_crypto_trading_1_1_logic_1_1_currency_logic.html", null ]
      ] ]
    ] ],
    [ "CryptoTrading.Logic.ILogic< Member >", "interface_crypto_trading_1_1_logic_1_1_i_logic.html", [
      [ "CryptoTrading.Logic.IMemberLogic", "interface_crypto_trading_1_1_logic_1_1_i_member_logic.html", [
        [ "CryptoTrading.Logic.MemberLogic", "class_crypto_trading_1_1_logic_1_1_member_logic.html", null ]
      ] ]
    ] ],
    [ "CryptoTrading.Logic.ILogic< Trade >", "interface_crypto_trading_1_1_logic_1_1_i_logic.html", [
      [ "CryptoTrading.Logic.ILogicTrade", "interface_crypto_trading_1_1_logic_1_1_i_logic_trade.html", [
        [ "CryptoTrading.Logic.TradeLogic", "class_crypto_trading_1_1_logic_1_1_trade_logic.html", null ]
      ] ]
    ] ],
    [ "CryptoTrading.Repository.IRepository< T >", "interface_crypto_trading_1_1_repository_1_1_i_repository.html", [
      [ "CryptoTrading.Repository.DBRepository< T >", "class_crypto_trading_1_1_repository_1_1_d_b_repository.html", null ]
    ] ],
    [ "CryptoTrading.Repository.IRepository< Crypto >", "interface_crypto_trading_1_1_repository_1_1_i_repository.html", [
      [ "CryptoTrading.Repository.ICryptoRepository", "interface_crypto_trading_1_1_repository_1_1_i_crypto_repository.html", [
        [ "CryptoTrading.Repository.CryptoRepository", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html", null ]
      ] ]
    ] ],
    [ "CryptoTrading.Repository.IRepository< Currency >", "interface_crypto_trading_1_1_repository_1_1_i_repository.html", [
      [ "CryptoTrading.Repository.ICurrencyRepository", "interface_crypto_trading_1_1_repository_1_1_i_currency_repository.html", [
        [ "CryptoTrading.Repository.CurrencyRepository", "class_crypto_trading_1_1_repository_1_1_currency_repository.html", null ]
      ] ]
    ] ],
    [ "CryptoTrading.Repository.IRepository< Member >", "interface_crypto_trading_1_1_repository_1_1_i_repository.html", [
      [ "CryptoTrading.Repository.IMemberRepository", "interface_crypto_trading_1_1_repository_1_1_i_member_repository.html", [
        [ "CryptoTrading.Repository.MemberRepository", "class_crypto_trading_1_1_repository_1_1_member_repository.html", null ]
      ] ]
    ] ],
    [ "Models.Member", "class_models_1_1_member.html", null ],
    [ "CryptoTrading.Program.Program", "class_crypto_trading_1_1_program_1_1_program.html", null ],
    [ "Models.Trade", "class_models_1_1_trade.html", null ],
    [ "Models.TradeItemBase", "class_models_1_1_trade_item_base.html", [
      [ "Models.Crypto", "class_models_1_1_crypto.html", null ],
      [ "Models.Currency", "class_models_1_1_currency.html", null ]
    ] ]
];