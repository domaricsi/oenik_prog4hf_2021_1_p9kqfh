var class_crypto_trading_1_1_logic_1_1_member_logic =
[
    [ "MemberLogic", "class_crypto_trading_1_1_logic_1_1_member_logic.html#acc09fa9787a5d11e4183170dede7c578", null ],
    [ "Add", "class_crypto_trading_1_1_logic_1_1_member_logic.html#ac1d8cba1912c5cf1a4752c97f94b4208", null ],
    [ "AddNew", "class_crypto_trading_1_1_logic_1_1_member_logic.html#ae87576035363d6c828cbb871b7fb255b", null ],
    [ "Btc", "class_crypto_trading_1_1_logic_1_1_member_logic.html#ab02ca80f886158a4f20ff09a7caef910", null ],
    [ "Buy", "class_crypto_trading_1_1_logic_1_1_member_logic.html#a5f88f83e151a2af5ea3df27f257ebeda", null ],
    [ "DeleteMember", "class_crypto_trading_1_1_logic_1_1_member_logic.html#afe2d5012afc388057b17a9ecad5c0529", null ],
    [ "Deposite", "class_crypto_trading_1_1_logic_1_1_member_logic.html#adcb084f653d42cbe68b2812e4cb7745d", null ],
    [ "GetAllNameAsString", "class_crypto_trading_1_1_logic_1_1_member_logic.html#a325ac182963847014bf59cab22bfbd7e", null ],
    [ "MyMoney", "class_crypto_trading_1_1_logic_1_1_member_logic.html#a195ac0407b6689c908b0d676440174d9", null ],
    [ "Read", "class_crypto_trading_1_1_logic_1_1_member_logic.html#aef4678ed302e85808dcabd979509c2d8", null ],
    [ "Register", "class_crypto_trading_1_1_logic_1_1_member_logic.html#abe57726182de584e1ceb63f3d2c8ce35", null ],
    [ "Remove", "class_crypto_trading_1_1_logic_1_1_member_logic.html#aaae89fcdc6b953bf42ff95b4b779ae8c", null ],
    [ "Sell", "class_crypto_trading_1_1_logic_1_1_member_logic.html#ac0596b7019fc5580fe61d740a351e533", null ],
    [ "Update", "class_crypto_trading_1_1_logic_1_1_member_logic.html#a55a46b06903e39a6a5f644bdd5afc817", null ]
];