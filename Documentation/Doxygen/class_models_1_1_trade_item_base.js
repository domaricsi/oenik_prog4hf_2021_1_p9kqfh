var class_models_1_1_trade_item_base =
[
    [ "TradeItemBase", "class_models_1_1_trade_item_base.html#aecae3b647a4e134f206445254bced25e", null ],
    [ "Currencies", "class_models_1_1_trade_item_base.html#a049e293f9d5dfa757c50f20b90049784", null ],
    [ "Name", "class_models_1_1_trade_item_base.html#ae106ddc222aa1d6ba6b27bbdc134f8e0", null ],
    [ "ShortName", "class_models_1_1_trade_item_base.html#a1c1e7f0eb85a14a1da5e765790432850", null ],
    [ "Trades", "class_models_1_1_trade_item_base.html#a2033e4c51cc375d4e9ae8d3e1372b724", null ],
    [ "Value", "class_models_1_1_trade_item_base.html#a3a1b20d968625d7bb158949d90fd724f", null ],
    [ "ValueToOneUSD", "class_models_1_1_trade_item_base.html#a663c832446b7869c436a7534e1d99876", null ]
];