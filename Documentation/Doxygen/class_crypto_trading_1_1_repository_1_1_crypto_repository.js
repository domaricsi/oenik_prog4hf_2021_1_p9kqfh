var class_crypto_trading_1_1_repository_1_1_crypto_repository =
[
    [ "CryptoRepository", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#a11542885995b88872e14d2a57fcd2b43", null ],
    [ "AddNewCrypto", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#a5af62ca24ce6f90db5b029ad69485f71", null ],
    [ "BuyBTC", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#a0e74fde9a513bd1604963e2f0f05ed3e", null ],
    [ "DoubleOrQuits", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#aba65ea429fe2a383736705e7734f87f1", null ],
    [ "GetOne", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#aff495608eec63a800dee6a2a9727f5a2", null ],
    [ "PossibleDelet", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#a84a9095819a562bed9973fdee5c31435", null ],
    [ "Refresh", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#a687c6d35ce5ada409881113f9224d775", null ],
    [ "SellBTC", "class_crypto_trading_1_1_repository_1_1_crypto_repository.html#abf396a7cc26e60b332f80375b43e4728", null ]
];