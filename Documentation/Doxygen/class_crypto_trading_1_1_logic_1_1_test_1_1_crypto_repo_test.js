var class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test =
[
    [ "AddingNewCrypto", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#a456c6ad6780b9a06d4dd6f4bd3c7cb86", null ],
    [ "Init", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#add71328bc4929c566ac9a8ece7536ab1", null ],
    [ "TestBuyBtc", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#afccdbe6fd7bd45e608dc6c93751b6f6a", null ],
    [ "TestDoubleOrQuits", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#a27cfb07205d13571c1e8b244993ac17c", null ],
    [ "TestGetAll", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#a4205f550f31ac48dead213dd7805114d", null ],
    [ "TestGetOne", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#adb1cd56d0afd4e6de1960f5eea2d1541", null ],
    [ "TestPossibleDelet", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#acdd9bb86818c32681f4c564895383392", null ],
    [ "TestRefresh", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#a1435764fd4ac54c7980a87ba038d89c8", null ],
    [ "TestSellBtc", "class_crypto_trading_1_1_logic_1_1_test_1_1_crypto_repo_test.html#a31ba7f005b9133bc03b4b706705d1b80", null ]
];