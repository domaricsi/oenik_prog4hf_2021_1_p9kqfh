var interface_crypto_trading_1_1_repository_1_1_i_repository =
[
    [ "Add", "interface_crypto_trading_1_1_repository_1_1_i_repository.html#af2e3e8dca3e2592538e486536e45a6cd", null ],
    [ "Delete", "interface_crypto_trading_1_1_repository_1_1_i_repository.html#a419a8b808b6d70b8057ee029078bc186", null ],
    [ "GetAll", "interface_crypto_trading_1_1_repository_1_1_i_repository.html#a287b59f0877acf1c404e8fefa6d40aad", null ],
    [ "GetOne", "interface_crypto_trading_1_1_repository_1_1_i_repository.html#a19e738a467924d34cb68480a4b5dd8dc", null ],
    [ "Update", "interface_crypto_trading_1_1_repository_1_1_i_repository.html#ad176fe4bf3e08229926083b038196853", null ]
];